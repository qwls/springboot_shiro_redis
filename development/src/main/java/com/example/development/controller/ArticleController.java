package com.example.development.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.development.model.User;
import com.example.development.service.ArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

/**
 * Created with IDEA
 * author:QinWei
 * Date:2019/4/19
 * Time:11:35
 */
@RestController
@Api(value = "用户Controller", tags = { "用户访问接口" })
public class ArticleController extends BaseController{

    @Autowired
    private ArticleService articleService;

    @ApiOperation(value = "测试")
    @GetMapping("/")  //映射hello请求
    @ApiResponses(value = { @ApiResponse(code = 1000, message = "成功"), @ApiResponse(code = 1001, message = "失败"),
            @ApiResponse(code = 1002,message = "缺少参数") })
    public String home(){
        LinkedList linkedList = new LinkedList();
        linkedList.add("DS");
        return  "Hello SpringBoots";
    }

    @ApiOperation(value = "获取用户信息列表")
    @PostMapping("findAll")
    @ApiResponse(code = 1000, message = "成功")
    public List<User> findAll(){
        return articleService.findAll();
    }


    @ApiOperation(value = "分页测试")
    @GetMapping("findAll/{state}/{currPage}")
    @ApiResponses ( value = {@ApiResponse(code = 1000, message = "成功"),@ApiResponse(code = 1002,message = "缺少参数")})
    public IPage<User> findAll(@PathVariable Integer state, @PathVariable Integer currPage ){

        Page page = new Page(currPage,10);
        return articleService.selectUserPage(page,state);
    }
}
