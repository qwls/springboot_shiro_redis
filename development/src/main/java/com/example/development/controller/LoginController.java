package com.example.development.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.development.common.ResultMap;
import com.example.development.common.ShiroKit;
import com.example.development.config.redis.IRedisService;
import com.example.development.model.User;
import com.example.development.service.LoginService;
import com.example.development.service.UserService;
import com.google.code.kaptcha.Producer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;
import org.apache.commons.io.IOUtils;
/**
 * Created with IDEA
 * author:QinWei
 * Date:2019/4/23
 * Time:10:29
 */
@RestController
@Api(value = "用户Controller", tags = { "用户登陆接口" })
public class LoginController extends BaseController{

        @Autowired
        private LoginService loginService;

        @Autowired
        private UserService userService;
        @Autowired
        private Producer producer;

        @Autowired
        private IRedisService redisService;
        //30分钟过期
        private final int EXPIRE = 1800;

        @ApiOperation(value = "验证码")
        @GetMapping("/captcha.jpg")
        public void kaptcha(HttpServletResponse response, HttpSession session) throws IOException {
            //生成文字验证码
            String code = producer.createText();
            //redisService.set(uuid, code);
            //session.setAttribute("code",code);
            redisService.set("code", code,300);
            response.setHeader("Cache-Control", "no-store,no-cache");
            response.setContentType("image/jpeg");

            BufferedImage image = producer.createImage(code);
            ServletOutputStream outputStream = response.getOutputStream();
            ImageIO.write(image, "jpg", outputStream);
            IOUtils.closeQuietly(outputStream);
        }


        @ApiOperation(value = "登陆测试")
        @GetMapping("/login")
        public ResultMap login(String username, String password,String kaptcha){
            logger.info("POST请求登录");
            if (StringUtils.isBlank(username)) {
                return ResultMap.error("用户名不能为空");
            }
            if (StringUtils.isBlank(password)) {
                return ResultMap.error("密码不能为空");
            }
            String validateCode = (String) redisService.get("code");
            if(StringUtils.isBlank(kaptcha) || StringUtils.isBlank(validateCode) || !kaptcha.equalsIgnoreCase(validateCode)){
                return ResultMap.error("验证码错误");
            }
            // 从SecurityUtils里边创建一个 subject
            Subject subject = SecurityUtils.getSubject();
            // 在认证提交前准备 token（令牌）
            UsernamePasswordToken token = new UsernamePasswordToken(username, password);
            // 执行认证登陆
            subject.login(token);
            //根据权限，指定返回数据
           User user = loginService.selectByLoginName(username,password);
        if (null == user) {
            return ResultMap.error("账号不存在");
        }
        //if (!user.getPassword().equals(ShiroKit.md5(password, user.getLoginName() + user.getSalt()))) {
        //    return ResultMap.error("密码不正确");
        //}
            //if ("user".equals(user.getRoleName())) {
            //    return ResultMap.ok().put("code","0");
            //}
            //if ("admin".equals(user.getRoleName())) {
            //    return ResultMap.ok().put("code","1");
            //}
            return ResultMap.ok().put("code","登陆成功！");
        }

        @ApiOperation(value = "退出")
        @PostMapping("/sys/logout")
        public ResultMap logout() {
            Subject subject = SecurityUtils.getSubject();
            //注销
            subject.logout();
            return ResultMap.ok();
        }

        @ApiOperation(value = "注册测试")
        @PostMapping("/register")
        public ResultMap register(String userName,String password,String loginName){
            User user = new User();
            user.setUserName(userName);
            user.setPassword(password);
            user.setLoginName(loginName);
            userService.insertUser(user);
            return ResultMap.ok();
        }


    @ApiOperation(value = "redis缓存测试")
    @PostMapping("/redis_user")
        public ResultMap redisUser(Integer pages){
        Object user1 = redisService.get("user"+pages);
        if(null ==user1){
            Page<User> page = new Page<User>(pages,10);
            Page pa = userService.redisUser(page);
            redisService.set("user"+pages,pa,300);
            return ResultMap.ok().put("code",pa);
        }

        return ResultMap.ok().put("code",user1);
    }
}
