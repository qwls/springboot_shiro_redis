package com.example.development.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.development.model.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created with IDEA
 * author:QinWei
 * Date:2019/4/23
 * Time:10:36
 */
@Mapper
public interface UserMapper {

    public User selectByLoginName(String loginName,String password);

    public String getRole(String loginName);

    public void insertUser(User user);

    public Page<User> redisUser(Page<User> page);
}
