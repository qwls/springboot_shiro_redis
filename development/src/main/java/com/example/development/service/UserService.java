package com.example.development.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.development.mapper.UserMapper;
import com.example.development.model.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created with IDEA
 * author:QinWei
 * Date:2019/4/25
 * Time:10:38
 */
@Service
public class UserService {

    @Resource
    private UserMapper userMapper;

    public void insertUser(User user){
        userMapper.insertUser(user);
    }

    public Page<User> redisUser(Page<User> page){
        return userMapper.redisUser( page);
    }
}
