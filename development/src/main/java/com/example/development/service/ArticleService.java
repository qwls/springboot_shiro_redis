package com.example.development.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.development.mapper.ArticleMapper;
import com.example.development.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IDEA
 * author:QinWei
 * Date:2019/4/19
 * Time:11:32
 */
@Service
@CacheConfig(cacheNames = "articleCache")
public class ArticleService {

    @Resource
    private ArticleMapper articleMapper;

    public List<User> findAll(){
        return articleMapper.selectAll();
    }


    public IPage<User> selectUserPage(Page<User> page, Integer state) {
        // 不进行 count sql 优化，解决 MP 无法自动优化 SQL 问题，这时候你需要自己查询 count 部分
        // page.setOptimizeCountSql(false);
        // 当 total 为非 0 时(默认为 0),分页插件不会进行 count 查询
        // 要点!! 分页返回的对象与传入的对象是同一个
        return articleMapper.selectPageVo(page, state);
    }
}
