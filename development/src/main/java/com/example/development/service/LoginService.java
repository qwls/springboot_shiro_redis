package com.example.development.service;

import com.example.development.mapper.UserMapper;
import com.example.development.model.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created with IDEA
 * author:QinWei
 * Date:2019/4/23
 * Time:10:30
 */
@Service
public class LoginService {

    @Resource
    private UserMapper userMapper;

    public User selectByLoginName(String loginName,String password){
        return userMapper.selectByLoginName(loginName,password);
    }

    public String getRole(String loginName){
        return userMapper.getRole(loginName);
    }
}
