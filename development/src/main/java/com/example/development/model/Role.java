package com.example.development.model;

import java.io.Serializable;

/**
 * Created with IDEA
 * author:QinWei
 * Date:2019/4/23
 * Time:10:11
 */
public class Role implements Serializable {


    private static final long serialVersionUID = 7447496232311613087L;

    private Integer id;

    private String roleName;

    private Integer userId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
